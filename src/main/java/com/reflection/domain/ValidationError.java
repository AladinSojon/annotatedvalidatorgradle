package com.reflection.domain;

/**
 * @author aladin
 * @since 2/6/20
 */
public class ValidationError {
    private String errors;

    public ValidationError(String errors) {
        this.errors = errors;
    }
    public String getErrors() {
        return errors;
    }
    public void setErrors(String errors) {
        this.errors = errors;
    }
}
