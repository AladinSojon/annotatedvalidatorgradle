package com.reflection.domain;

/**
 * @author aladin
 * @since 2/6/20
 */
public class Person {
    @Size(max = 10)
    private String name;
    @Size(min = 18, message = "Age can not be less than {min}")
    private int age;
    public Person(String s, int i) {
        name = s;
        age = i;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }   
    public void setAge(int age) {
        this.age = age;
    }

}
