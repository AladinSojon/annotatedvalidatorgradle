package com.reflection.domain;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * @author aladin
 * @since 2/6/20
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Size {
    int max() default 100;

    int min() default 1;

    String message() default "Length must be {min}-{max}";

}
