package com.reflection.services;

import com.reflection.domain.Person;
import com.reflection.domain.Size;
import com.reflection.domain.ValidationError;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

/**
 * @author aladin
 * @since 2/6/20
 */
public class AnnotatedValidatorServices {
    public static void validate(Person p, List<ValidationError> errors) {

        List<Field> privateFields = new ArrayList<Field>();
        Field[] allFields = Person.class.getDeclaredFields();
        for (Field field : allFields) {
            Annotation annotation = field.getAnnotation(Size.class);
            
            if(annotation instanceof Size) {
                Size myAnnotation = (Size) annotation;

                if(int.class.isAssignableFrom(field.getType())) {
                    if(p.getAge()<myAnnotation.min()) {
                        errors.add(new ValidationError("Age can not be less than "+String.valueOf(myAnnotation.min())));
                    }
                }
                else if(String.class.isAssignableFrom(field.getType())) {
                    if(p.getName().length()>myAnnotation.max()) {
                        errors.add(new ValidationError("Name length must be 1 - "+String.valueOf(myAnnotation.max())));
                    }
                }
            }
        }

    }

    public static void print(List<ValidationError> errors) {
        for(ValidationError error : errors) {
            System.out.println(error.getErrors());
        }
    }
}
